import colorama
import random
import time
import os

# Function to clear the screen
def clear_screen():
    os.system('cls' if os.name == 'nt' else 'clear')

# Function to generate random stars
def generate_stars(width, height):
    stars = []
    for _ in range(height):
        row = []
        for _ in range(width):
            if random.random() < 0.04:  # Adjust density of stars
                row.append('*' if random.random() < 0.5 else '.' if random.random() < 0.5 else '+')
            else:
                row.append(' ')
        stars.append(row)
    return stars

# Function to print the animation
def print_animation(stars, text, colors):
    clear_screen()
    for row in range(len(stars)):
        for col in range(len(stars[row])):
            color = random.choice(colors)
            print(color + stars[row][col], end='')
        if row == len(stars) // 2:
            color = random.choice(colors)
            print(color + text.center(os.get_terminal_size().columns))
        print()

# Main function
def run():
    colorama.init()
    colors = [colorama.Fore.RED, colorama.Fore.GREEN, colorama.Fore.YELLOW, colorama.Fore.BLUE, colorama.Fore.MAGENTA, colorama.Fore.CYAN]
    width, height = os.get_terminal_size().columns, os.get_terminal_size().lines
    stars = generate_stars(width, height)
    text = "YOU WON"

    counter = 25
    while True:
        # generate stars every 10 cycles only
        if random.random() < 0.1:
            stars = generate_stars(width, height)

        print_animation(stars, text, colors)
        time.sleep(0.2)

        counter -= 1
        if counter < 1:
            break

if __name__ == "__main__":
    run()
