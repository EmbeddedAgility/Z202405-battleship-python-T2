from behave import given, when, then
from torpydo.ship import Color, Letter, Ship
from torpydo.battleship import parse_position
from torpydo.game_controller import GameController

@given('I have a {ship_length:d} ship with {positions:d} positions')
def step_impl(context, ship_length, positions):
    context.ship = Ship('Test', ship_length, Color.RED)

    for i in range(positions):
        context.ship.add_position(f"A{i+1}")

@when('I check if the ship is valid')
def step_impl(context):
    context.success = str(len(context.ship.positions) == context.ship.size)

@then('the result should be {result}')
def step_impl(context, result):
    assert context.success == result

@given('I have a board of 8x8 and I have shot at {point:w}')
def step_impl(context, point):
    context.position = parse_position(point)

@when('I check if the shot is valid')
def step_impl(context):
    context.success = str(GameController.is_valid_shot(context.position))

@given('I have no fleet and enemy has a {enemyColor} {enemyName} with {enemyLength} length fleet')
def step_impl(context, enemyName, enemyColor, enemyLength):
    context.myFleet = []
    context.enemyFleet = [Ship(enemyName, enemyLength, enemyColor)]

@given('I have a {myColor} {myName} with {myLength} length fleet and enemy has no fleet')
def step_impl(context, myName, myColor, myLength):
    context.myFleet = [Ship(myName, myLength, myColor)]
    context.enemyFleet = []

@given('I have a {myColor} {myName} with {myLength:d} length fleet and enemy has a {enemyColor} {enemyName} with {enemyLength:d} length fleet')
def step_impl(context, myName, myColor, myLength, enemyName, enemyColor, enemyLength):
    context.myFleet = [Ship(myName, myLength, myColor)]
    context.enemyFleet = [Ship(enemyName, enemyLength, enemyColor)]

    for i in range(myLength):
        context.myFleet[0].add_position(f"A{i+1}")
    
    for i in range(enemyLength):
        context.enemyFleet[0].add_position(f"A{i+1}")

@when('I shoot {position}')
def step_impl(context, position):
   GameController.remove_position(context.enemyFleet, parse_position(position))

@when('I check if the game is finished')
def step_impl(context):
    if len(context.myFleet)==0:
        context.success = "Lose"
    elif len(context.enemyFleet)==0:
        context.success = "Win"
    else:
        context.success = "Continue"
@given('a fleet with a {ship_length:d} ship')
def step_impl(context, ship_length):
    context.ship = Ship('Test', ship_length, Color.RED)
    if not hasattr(context, 'fleet'):
        context.fleet = []
    context.fleet.append(context.ship)

@given('the ship is placed at positions {positions}')
def step_impl(context, positions):
    for pos in positions.split(","):
        context.ship.add_position(pos.strip())

@when('the ship is hit at positions {hit_positions}')
def step_impl(context, hit_positions):
    context.hit_results = []
    for pos in hit_positions.split(","):
        position = parse_position(pos.strip())
        hit_result = GameController.check_is_hit(context.fleet, position)
        context.hit_results.append(hit_result)

@then('the ship should be reported as {status}')
def step_impl(context, status):
    expected_status = status == 'sunk'
    actual_status = context.ship.is_sunk()
    assert actual_status == expected_status, f"Expected {expected_status} but got {actual_status}"

@then('the fleet status should be displayed as')
def step_impl(context):
    expected_status = [line.strip() for line in context.text.strip().split('\n')]
    actual_status = []
    for ship in context.fleet:
        status = "sunk" if ship.is_sunk() else "afloat"
        actual_status.append(f"{ship.name}: {status}")

    assert actual_status == expected_status, f"Expected {expected_status} but got {actual_status}"
