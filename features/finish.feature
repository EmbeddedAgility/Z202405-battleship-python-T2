Feature:IsGameFinish


Scenario: Game Continues
    Given I have a CADET_BLUE Aircraft Carrier with 5 length fleet and enemy has a CADET_BLUE Aircraft Carrier with 5 length fleet
    When I check if the game is finished
    Then the result should be Continue

Scenario: Player Loses
    Given I have no fleet and enemy has a CADET_BLUE Aircraft Carrier with 5 length fleet
    When I check if the game is finished
    Then the result should be Lose

Scenario: Player Win
    Given I have a CADET_BLUE Aircraft Carrier with 5 length fleet and enemy has no fleet
    When I check if the game is finished
    Then the result should be Win

Scenario: Player Win after playing
    Given I have a CADET_BLUE Aircraft Carrier with 5 length fleet and enemy has a CADET_BLUE Aircraft Carrier with 2 length fleet
    When I shoot A1
    When I shoot A2
    When I check if the game is finished
    Then the result should be Win

