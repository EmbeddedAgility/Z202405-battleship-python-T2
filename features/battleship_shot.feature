Feature: IsShotValid
	In order to avoid cheating
	As a player
	I want to be notified if my shot was in an invalid position

Scenario: Valid shot placement - 1
    Given I have a board of 8x8 and I have shot at A5
    When I check if the shot is valid
    Then the result should be True

Scenario: Invalid shot placement - 2
    Given I have a board of 8x8 and I have shot at A7
    When I check if the shot is valid
    Then the result should be True

Scenario: Valid shot placement - 3
    Given I have a board of 8x8 and I have shot at H8
    When I check if the shot is valid
    Then the result should be True

Scenario: Invalid shot placement - 1
    Given I have a board of 8x8 and I have shot at A20
    When I check if the shot is valid
    Then the result should be False

Scenario: Invalid shot placement - 2
    Given I have a board of 8x8 and I have shot at A9
    When I check if the shot is valid
    Then the result should be False

Scenario: Invalid shot placement - 3
    Given I have a board of 8x8 and I have shot at H9
    When I check if the shot is valid
    Then the result should be False

