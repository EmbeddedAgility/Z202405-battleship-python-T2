Feature: IsShipValid
	In order to avoid cheating
	As a player
	I want to be notified if my ship has a invalid placement

Scenario: Valid ship placement
    Given I have a 5 ship with 5 positions
    When I check if the ship is valid
    Then the result should be True

Scenario: Invalid ship placement
    Given I have a 5 ship with 4 positions
    When I check if the ship is valid
	Then the result should be False

Scenario: Ship is afloat
    Given a fleet with a 3 ship
    And the ship is placed at positions A1, A2, A3
    When the ship is hit at positions A1
    Then the ship should be reported as afloat
    And the fleet status should be displayed as
        """
        Test: afloat
        """

Scenario: Ship is sunk
    Given a fleet with a 3 ship
    And the ship is placed at positions A1, A2, A3
    When the ship is hit at positions A1, A2, A3
    Then the ship should be reported as sunk
    And the fleet status should be displayed as
        """
        Test: sunk
        """

Scenario: Mixed fleet status
    Given a fleet with a 3 ship
    And the ship is placed at positions A1, A2, A3
    And a fleet with a 2 ship
    And the ship is placed at positions B1, B2
    When the ship is hit at positions A1, A2, B1, B2
    Then the fleet status should be displayed as
        """
        Test: afloat
        Test: sunk
        """
